import unittest
from selenium import webdriver


class NewVisitorTest(unittest.TestCase):
	def setUp(self):
		self.browser = webdriver.Firefox()
		self.browser.implicitly_wait(3) # attendre le chargement des pages

		
	def tearDown(self):
		self.browser.quit()
		
	def test_can_start_a_list_and_retrieve_it_later(self): 
		# Edith has heard about a cool new online series manager app. She goes
		# to check out its homepage
		self.browser.get('http://localhost:9090')
		
		# She notices the page title and header mention series lists
		self.assertIn('series', self.browser.title)
		self.fail('Finish the test!')
		
		# She is invited to enter a serie item straight away
		# She types "Vampires Diaries" into a text box (Edith's hobby
		# is tying fly-fishing lures)
		# When she hits enter, the page updates, and now the page lists
		# "1: Vampire Diaries" as an item in a series list
		# There is still a text box inviting her to add another item. She
		# enters "Hannibal"
		# The page updates again, and now shows both items on her list
		# Edith wonders whether the site will remember her list. Then she sees
		# that the site has generated a unique URL for her -- there is some
		# explanatory text to that effect.
		# She visits that URL - her series list is still there.
		# Satisfied, she goes back to sleep

if __name__ == '__main__':
		unittest.main()
